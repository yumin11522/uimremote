package com.test.uimremote.sim;

/**
 * TODO Add class comment here
 *
 * @author zhupeizhen
 * 2020/11/20
 */
public class SimAccessAuthBean {

    private int MCC; // MCC，Mobile Country Code，移动国家代码（中国的为460）；
    private int MNC; // MNC，Mobile Network Code，移动网络号码（中国移动为00，中国联通为01）；
    private int CID; // CID，Cell Identity，基站编号，是个16位的数据（范围是0到65535）。
    private int LAC; // LAC，Location Area Code，位置区域码；
    private int FirmwareVer;
//    private int MIP_ListVer;
    private String SN; //
//    private int data_Size;
//    private byte vir_flag;
//    private String take_time;
    private byte Battery;//电量
//    private byte flag;
    private String versionAPK; //当前apk软件版本
//    private String LastIMSI;//最后一次分配给设备的SIM的IMSI


    public int getMCC() {
        return MCC;
    }

    public void setMCC(int MCC) {
        this.MCC = MCC;
    }

    public int getMNC() {
        return MNC;
    }

    public void setMNC(int MNC) {
        this.MNC = MNC;
    }

    public int getCID() {
        return CID;
    }

    public void setCID(int CID) {
        this.CID = CID;
    }

    public int getLAC() {
        return LAC;
    }

    public void setLAC(int LAC) {
        this.LAC = LAC;
    }

    public int getFirmwareVer() {
        return FirmwareVer;
    }

    public void setFirmwareVer(int firmwareVer) {
        FirmwareVer = firmwareVer;
    }

    public String getSN() {
        return SN;
    }

    public void setSN(String SN) {
        this.SN = SN;
    }

    public byte getBattery() {
        return Battery;
    }

    public void setBattery(byte battery) {
        Battery = battery;
    }

    public String getVersionAPK() {
        return versionAPK;
    }

    public void setVersionAPK(String versionAPK) {
        this.versionAPK = versionAPK;
    }
}
