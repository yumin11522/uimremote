/**
 * com.test.uimremote
 * XmClient.java
 * 2020/9/17
 * Copyright (c) Genew Technologies 2010-2020. All rights reserved.
 */
package com.test.uimremote.jni;

/**
 * TODO Add class comment here<p/>
 *
 * @author xiongqm
 * @version 1.0.0
 * @history<br/> ver    date       author desc
 * 1.0.0  2020/9/17 xiongqm created<br/>
 * <p/>
 * @since 1.0.0
 */
public class XmClient {

    static public interface XmCallBack{
        void onData(byte[] data);
    };

    static public native boolean init(String domain, String ip, int port);

    static public native boolean start();

    static public native boolean stop();

    static public native void setCalback(XmCallBack callBack);

    static {
        System.loadLibrary("sim_jni");
    }
}
