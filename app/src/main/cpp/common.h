#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "p_lib.h"
#include "usim_drv.h"

#define	IMSI_LEN					9
#define MAX_LEN_CONTENT				3072
#define MAX_LEN_FCH					256
#define MAX_FILE_CODE_LEN			16	//=0x10
#define MAX_COMMENTS				16
#define NULL_POINT					0xFFFF
#define ATR_REBOOT_MAX_CNT			0
#define	TRANSFER_TRANSPARENT_IDEL	0
#define	TRANSFER_TRANSPARENT_START	1
#define	TRANSFER_TRANSPARENT_WAIT	2
#define	TRANSFER_TRANSPARENT_OVER	3
#define VUSIM_AUTH_DAT_RECVING 		1
#define VUSIM_AUTH_DAT_RECV_ERR 	2
#define VUSIM_AUTH_DAT_RECV_FINISH 	3
#define VSIM_AUTH_RECORD_MAX 		6
#define MAX_SND2SOCK_DAT_LEN 		256

#define HIST_FIRST_USIM				0x80	// the first of the historical character of USIM
#define HIST_SEC_USIM				0x31

#define	APDU_STAGE0 				0
#define	APDU_STAGE1 				1 //head process

#define	FCB_FILE 					"/data/meeyoo/current/FCB.dat"
#define	FCP_FILE 					"/data/meeyoo/current/FCP.dat"
#define	FCT_FILE 					"/data/meeyoo/current/FCT.dat"
#define	FCH_FILE 					"/data/meeyoo/current/FCH.dat"
#define	ATR_FILE 					"/data/meeyoo/current/ATR.dat"
#define DEVICE_NAME					"/dev/simuart"
#define ATR_REBOOT_CNT_FILE  		"/data/meeyoo/atr_reboot_cnt.dat"



#define USIM_CAL_TD_COUNT(data,index)	\
	{\
			if(data & TAMask) index++;\
	   		if(data & TBMask) index++;\
   			if(data & TCMask) index++;\
   	}

typedef unsigned char 		u8;

typedef enum
{
	UNKNOWN,
	USIM=1,
	SIM
}Sim_Type_enum;

typedef enum 
{
	LOW_LEVEL = 0,
	HIGH_LEVEL = 1,
	RISE_EDGE = 2,
	DOWN_EDGE = 3
}result_of_vusim_rst_scan;

typedef enum 
{
#if 1
	V_REMOVE = 0,
	V_INSERT = 1,
#else
	V_INSERT = 0,
	V_REMOVE = 1,
#endif
	V_INIT,
	V_RESET
}args_of_vusim_ctl;

typedef enum
{
	I2S_CMD_NULL = 0,
	I2S_CMD_UICC_AUTH,
	I2S_CMD_SOCK_RECONNECT,
}I2S_CMD_e;

typedef struct
{
	int Cmd;
	int DatLen;
	unsigned char Dat[MAX_SND2SOCK_DAT_LEN];
}InterfaceDat_2_Socket;

typedef struct
{
	u8 				comments[MAX_COMMENTS];//16
	u8 				code_len;//17
	u8 				code[MAX_FILE_CODE_LEN];//33
	u8 				typeH;//34
	u8 				typeL;//35
	u8 				FCP_pointerH;	//36
	u8 				FCP_pointerL;//37
	u8 				FCP_len;//38
	u8 				content_pointerH;//39
	u8 				content_pointerL;//40
	u8				content_len;//41
}FILE_CONTROL_BLOCK;

typedef struct
{
	u8 				Flag;
	u8 				Len;
	u8 				Buf[126];
}TT_s;

typedef struct
{
	u8 				cmd;
	u8 				code_len;
	u8 				SW[2];

	u8 				code[MAX_FILE_CODE_LEN];

	u8 				Le;
	u8 				Authenticate_Flag;
	u8 				Status_Flag;
	u8 				USAT_Flag;
	
	u8 				RXbuf[MAX_LEN_FCH];
	u8 				TXbuf[MAX_LEN_FCH];
	TT_s 			TT_Dat;
	
	int 			VsimType;

	int 			TX_len;
	int 			FCP_point;
	int 			FCP_len;
	int 			FCT_point;
	int 			FCT_len;
	int 			Current_DFType;
	int 			APDU_STAGE;
	int 			APDU_PROC_STEP;
}VUSIM_STA;

typedef struct
{
	int 			flag;

	u8 				Snd_Cnt;
	u8 				Rcv_Cnt;
	u8 				Snd_Len;
	u8 				Rcv_Len;

	u8 				SndBuf[256];
	u8 				RcvBuf[256];
	struct timeval 	snd_time;
	struct timeval 	rcv_time;
}TT_Ctl_t;

typedef struct _IMSI_Data
{
	unsigned char 	IMSI[10];
	char			sIMSI[20];
	TT_Data			Spm_Para;
}IMSI_Data;

typedef struct _Common_Data
{
	int				FCB_Size;	
	int				FCP_Size;	
	int				FCT_Size;	
	int				FCH_Size;	
	int				ATR_Len;
	int				Android_Socket_fd;
	int				Point_Vsim_Auth;
	int				State;//add 20160818

	u8 				File_FCB[MAX_LEN_CONTENT];
	u8 				File_FCP[MAX_LEN_CONTENT];
	u8 				File_FCT[MAX_LEN_CONTENT];
	u8 				File_FCH[MAX_LEN_FCH];

	u8				DEFAULT_ATR[34];//max len is 33
	u8				USAT_SW1;
	u8				USAT_SW2;

	APDU_HEAD		APDUhead;
	VUSIM_STA		VUSIM_Global;
	TT_Ctl_t		g_TT_ctl;

	TT_Data			Spm_Para;
	time_t 			g_TT_Success_time;
}Common_Data;


extern Common_Data Data;


extern int VUSIM_APDU_HeadDet(unsigned char *Buf, int iLen);
//extern void VUSIM_ReProcessByte(unsigned char INS);
extern void VUSIM_RetData(unsigned char *Buf, int lenth);
extern void VUSIM_FCT_UPDATE(VUSIM_STA *sta, unsigned char *buf, unsigned char len);
extern int TT_Dat_Pack1(unsigned char * pData,int iLen,unsigned char *Head, unsigned char *Dst);
extern int TT_Dat_Pack2(unsigned char *Src, int iLen, unsigned char cnt, unsigned char *Dst);
extern void APDU_Send_Byte_Ins(APDU_HEAD *head, VUSIM_STA *VS);
extern int VSIM_APDU_Para_Test(unsigned char *RecvBuf, int RecvLen, APDU_HEAD *head, int TimeOut);
extern int Set_Atr_Reboot_Cnt(int iCnt);
extern int Get_Atr_Reboot_Cnt(void);
extern int Get_Data_By_TimeOut(int Fd, int Second, int millSend);
extern char *hex2str(unsigned char *data, uint32 len);
#endif
