#ifndef __ACCESS_AUTH_H__
#define	__ACCESS_AUTH_H__





#define	RequestTimeOut    20 //second


#define FlagRead   0
#define FlagUpdate 1
#define FlagReadDirect 2

/*
the Macro definition for access authenticate of terminal device
*/
#define	AA_RESULT_STATE_SUCCESS		0x31
#define	AA_RESULT_STATE_FAIL		0x32
#define AA_RESULT_STATE_ERROR		0x33

#define	AA_RESULT_FILERQ_TRUE		0x01
#define	AA_RESULT_FILERQ_FALSE		0x00

#define AA_ERR_SN_NOT_EXIST			0x01
#define AA_ERR_CHARGES_OWED			0x02
#define AA_ERR_VIOLATE_AA_INFO		0x03
#define AA_ERR_NO_SIM_AVALIABLE		0x31
#define AA_ERR_CONNECT_SCM			0x32
#define AA_ERR_SENDTO_SCM			0x33
#define AA_ERR_RECVFROM_SCM			0x34
#define AA_ERR_SENDTO_MS			0x35
#define AA_ERR_RECVFROM_MS_TIMEOUT	0x36
#define AA_ERR_RECVFROM_MS			0x37
#define AA_ERR_SERVER_ABNORMAL		0x38
#define AA_ERR_UNKNOWN				0x39

#define ACCESS_AUTH_FINISH			1
#define ACCESS_AUTH_FAIL_TIMEOUT	2

#define ACCESS_AUTH_FAIL_SEND_ERRO  3
#define ACCESS_AUTH_FAIL_RECV_ERRO	4
extern int Clr_CellData(void);

#endif
