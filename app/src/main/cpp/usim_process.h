#ifndef __USIM_PROCESS_H__
#define	__USIM_PROCESS_H__
#include "p_lib.h"

#include "usim_drv.h"
#include "common.h"

typedef enum
{
	TAG_AID					=0x4F,
	TAG_APPLICATION			=0x61,
	TAG_FCP					=0x62,
	TAG_FILE_SIZE			=0x80,	
	TAG_FILE_ALL_SIZE		=0x81,
	TAG_FILE_DESCRIPTION	=0x82,
	TAG_FILE_ID		  		=0x83,	
	TAG_AID_IND				=0x84,
	TAG_LIFE				=0x8A,
	TAG_SECURE1				=0x86,	
	TAG_SECURE2	 			=0x8B,
	TAG_SECURE3 			=0x8C,
	TAG_SECURE4				=0xAB,

	TAG_PRIVATE				=0xA5,
	TAG_PIN				  	=0xC6,
	TAG_SFI					=0x88,	
}EF_TAG;

typedef	enum
{
	USIM_MF					=0x0000,
	USIM_ADF				=0x1000,

	USIM_MF_DF				=0x0100,
	USIM_MF_EF				=0x0001,
	USIM_MF_DF_DF			=0x0110,
	USIM_MF_DF_DF_EF		=0x0111,

	USIM_ADF_EF				=0x1001,
	USIM_ADF_DF				=0x1100,
	USIM_ADF_DF_DF			=0x1110,
	USIM_ADF_DF_EF			=0x1101,

	NOT_USIM_FILE			=0x2000
} FILE_TYPE;

typedef enum{
	NO_ACTION,
	COMMAND_ONLY,
	COMMAND_PEROIDLY,				
	SELECT_ONLY,				//P1=0x00,P2=0x0c(no data back)
	SELECT_GET,					//P1=0x00,P2=0x04(need FCP back)
	SELECT_GET_READ,			//B0: binray, B2:record
	SELECT_GET_UPDATE,			//D6:undate binary,DC:update record
}FILE_ACTION;

typedef struct
{
	unsigned char 			cmd;
	unsigned char 			code[MAX_FILE_CODE_LEN];
	FILE_TYPE 				type;
	FILE_ACTION 			action;
	char 					comments[MAX_COMMENTS];
}ACTION;
/*********************************************************************/

typedef struct {
	unsigned char SF; //短文件标识
	int FID; //文件标识
}SFI_LIST;

typedef struct 
{
	unsigned char 			tag;
	unsigned char 			len;
	unsigned char 			value[50];
}tlv_def;

#define MAX_ADF_EF_SFI_LIST 200

extern int USIM_MF_EF_SFI_Num ;
extern int USIM_ADF_EF_SFI_Num ;
extern int USIM_ADF_DF_EF_SFI_Num ;
extern SFI_LIST USIM_ADF_EF_SFI_DEFAULT_LIST[32];
extern SFI_LIST USIM_ADF_EF_SFI_LIST[MAX_ADF_EF_SFI_LIST];
extern int VUSIM_MAIN_PROC(unsigned char *Buf, int iLen, int *flag);

#endif
