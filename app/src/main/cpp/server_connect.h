#ifndef __SERVER_CONNECT_H__
#define __SERVER_CONNECT_H__

#define CONCT_ERR_NEED_SWAP_IP -1
#define CONCT_ERR_SWAP_IP_BY_CNT -2
#define CONCT_ERR_SWAP_IP_BY_TIME -3
#define IP_LEN 16
#define DOMAIN_NAME_LEN 40
extern pthread_mutex_t mutex_sock_fd;
extern Sys_Ctl_t SysCtl;
extern int AplicationConnectToServer(void);
extern int SetServerInfo(Server_Port_Ip *para);//add by lk 20150518 
extern int TCP_IP_Main(int socket_fd);//add by lk 20150518
extern int DstANL_IP(char *Src, Server_Port_Ip *para);
extern int SetNetConfig(void);//add by lk 20150518
int serverInfoCal(void);

//extern int GetAddr_Str(char *Src, Server_Port_Ip *para);//add by lk 20150529

#endif
