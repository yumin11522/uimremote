#ifndef __P_LIB_H__
#define __P_LIB_H__

#define	RecvBufSize 	256
#define	SendBufSize 	1024

#if ANDROID
#include <jni.h>
#include <android/log.h>
#define TAG    "sim-jni" // 杩欎釜鏄嚜瀹氫箟鐨凩OG鐨勬爣璇?
#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,TAG,__VA_ARGS__) // 瀹氫箟LOGD绫诲瀷
//#define DBUG(fmt, arg...)  LOGD("%s:%d  "fmt"\n", __func__,__LINE__,##arg)
#define DBUG(fmt, arg...) log_printf("", "", 0, fmt, ##arg)
#else
#define DBUG(fmt, arg...) log_printf(__FILE__,__FUNCTION__, __LINE__, fmt, ##arg)
#endif








typedef long			LONGINT;
typedef int				POINTER;


typedef unsigned short 	uint16;
typedef unsigned int   	uint32;
typedef unsigned char	u8;



typedef struct
{
	unsigned char	IP[4];

	unsigned short	Port;
	unsigned char   system_id;
	unsigned char	channel_id;

	unsigned int	SN;
}TT_Data;




typedef struct
{
	unsigned char Cmd_TAG;
	unsigned char FrameSize;
	unsigned char Frame_Data[RecvBufSize];
}TD_ComProtocl_SendFrame_t;

typedef struct
{
	unsigned char Cmd_TAG;
	unsigned char FrameSizeL;
	unsigned char FrameSizeH;
    unsigned char Result;
	unsigned char Frame_Data[SendBufSize];
}TD_ComProtocl_RecvFrame_t;

extern int Hex2String(unsigned char * src,char * dst,int src_len);
extern int str2Hex(char * src,unsigned char * dst,int src_len);
extern int str_PickUp(char * Src,char * sStart,char * sEnd,char * Dst);
extern long FileCopy(char *From,char *To);
extern int Delay(long sec,long ms);
extern unsigned long get_file_size(const char *path);
extern void PrintDebuf(const char * msgInfo,const unsigned char *pucBuf,int iLen);
extern int FileCpWithCs(char *DstFile, char *SrcFile);
extern int File_Read(char *FileName, unsigned char *Dst, int iLen);
extern int File_Update(char *FileName, unsigned char *Src, int iLen, int iOffset);
extern int write_data(unsigned char *Buf, int iLen, int fd);
extern int read_data(unsigned char *Buf, int iLen, int iTimeOut, int fd);
/**
 * 打印日志
 * @param file __FILE__
 * @param function __FUNCTION__
 * @param line __LINE__
 * @param fmt printf类型的格式控制字符串
 * @param ... 不定长参数
 */
extern void log_printf(const char *file, const char *function, int line, const char *fmt, ...);

#endif



