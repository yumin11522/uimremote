#ifndef __INCLUDE_H_XM__
#define __INCLUDE_H_XM__

#include <unistd.h>
#include <sys/types.h>       /* basic system data types */
#include <sys/socket.h>      /* basic socket definitions */
#include <netinet/in.h>      /* sockaddr_in{} and other Internet defns */
#include <arpa/inet.h>       /* inet(3) functions */
#include <netdb.h> 		  /*gethostbyname function */
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h> 
#include <linux/types.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/select.h>
#include <termios.h>
#include <sys/reboot.h>
#include <pthread.h>
#include <signal.h>
#include "p_lib.h"
   
#define	FALSE						0
#define	TRUE						1
#define FAILED      				-1

#define TAG_IP						0x02//add by lk 20150518
#define TAG_ACCESS_AUTH				0x81
#define	TAG_SIMFILE_REQUEST			0x82
#define	TAG_CMDTT					0x83
#define	TAG_HEARTBEAT				0x84
#define	TAG_CFMD					0x85
#define	TAG_ADN						0x86
#define	TAG_LOCAL_STA				0x87
#define TAG_CONF_FILE_UPDATE		0x88
#define	TAG_SWAP_VUSIM				0x89
#define	TAG_GET_APN_INFO			0x8A
#define	TAG_HB_NEW					0x8B
#define TAG_DEAL					0xB0//add by lk 20151204

#define TAG_DEAL_RET				0xB1//add by lk 20151204
#define	TAG_TERMINAL_TURN_OFF		0xA5
#define	TAG_RESULT_ACCESS_AUTH		0x91
#define	TAG_RESULT_SIMFILE_REQUEST	0x92
#define	TAG_RESULT_CMDTT			0x93
#define	TAG_HB_RET					0x94
#define	TAG_HB_NEW_RET				0x9B
#define	TAG_LOCAL_STA_RESULT		0x97
#define TAG_GET_APN_INFO_RET		0x9A
#define TAG_LOGOUT_CMD				0x9C
#define TAG_APN_CMD 				0x9d
#define TAG_AUTONET                 0x9E//150714

#define TAG_LOG						0x41
#define TAG_UPDATE_LOCAL 			0x8c
#define TAG_UPDATE_ROAM 			0x8d
#define TAG_UPDATE_LOCAL_APK 		0x8e
#define TAG_UPDATE_ROAM_APK 		0x8f
#define TAG_UPDATE_MIP 				0x98
#define TAG_MSG_CMD 				0x99
#define TAG_SHUTDOWN_CMD            0xa0
#define TAG_RELOGIN_CMD             0xa1
#define TAG_REINSERT_VSIM_CMD       0xa2
#define TAG_UPDATE_ROAM_PAPK 		0xa7 

#define IMSI_LEN 					9
#define SN_LEN 						16
#define BUFF_LEN 					256
#define NORMAL_BUFF_LEN 			128
#define	MAX_Connect 				200
#define	RecvBufSize 				256
#define	SendBufSize 				1024
#define NORMAL_BUFF_MAX 			1024//add by lk 20150611


typedef struct 
{
	int 				Id;
	int 				RecvTimeout;
	int 				SendLen;
	unsigned char 		buf[BUFF_LEN];
}sock_msg_t;

typedef struct
{
	int 				SP_fd;
	int 				restartFlag;
	int 				net_3g_type;
	int 				net_3g_sta;
	int 				net_3g_sig_strenth;
	int 				Wifi_sta;
	unsigned int 		DataUsed;
	unsigned int 		rxBytes;
	unsigned int 		txBytes;
	int 				VUSIM_Activate_Result;
}Local_Module_Sta_t;

typedef struct Vsim_Logout_t
{
	char 		  		SN[SN_LEN];
	unsigned char 		Imsi[IMSI_LEN]; 
}Vsim_Logout;

typedef struct __result_sock_t
{
    unsigned char		SimFileUpdateFlag;
    unsigned char		ConfigFileUpdateFlag;
    unsigned char		FirmwareUpdateFlag;
	unsigned char 		Vusim_Active_Flag;   //ywy add 20150209
	unsigned char		Data_Clr_Flag;
	unsigned char		Unused[2];
	unsigned char		imsi[IMSI_LEN];
    unsigned short		threshold_data;//modify by lk 20151008
	unsigned short		dataSpeed;
	int					minute_Remain;

	unsigned short		byteRemain;
	unsigned char		system_id;//add by lk 20151009
	unsigned char   	channel_id;//add by lk 20151009

	unsigned char		IP[4]; //add by lk 20151008

	unsigned char   	reserve[4];//add by yyh 20160315
}TD_Result_t; //add by lk 20150204

typedef struct
{
	int					MCC;
	int 				MNC;
	int 				CID;
	int 				LAC;
	int 				FirmwareVer;
	unsigned int    	MIP_ListVer;
//	char 				MIP_List[128];

	char            	SN[16];

    unsigned short  	Data_Size;
    unsigned char   	vir_flag;
    unsigned char   	take_time[2];
    unsigned char   	Battery;

    unsigned char   	flag;

    unsigned char   	versionAPK[4];
    unsigned char   	LastIMSI[5];
}AccessAuth_Def;

typedef struct
{
	unsigned char		ErrCode;
	TD_Result_t     	AA_OK;
}AccessAuth_Result_t;

typedef struct Server_Port_Ip_t
{
    char 				IP[4][16];
    int 				Port[4];
	char 				domainName[40];
}Server_Port_Ip;//add by lk 20150529

typedef struct 
{
	int					Step;//add by 20161011
	int					Sockfd;//add by 20161011

	int					IsLogThread_Run_flag; //log flag 20161012
	unsigned int 		Timer_Run;
	int 				Heart_ret_old_flag;
	Server_Port_Ip 		ServerInfo;//add by lk 20150529
	unsigned int		ApkVersion;//add by 20161012
	int					Battery;//add by 20161012
	int					interval;//add by 20161014
	
	/*  *1*  bill thread ctl*/
	int					billStatus;
	int					DayMinutesRemain;
	unsigned int		DataRemain;
	
	/*  *2*  HB thread*/
	int					HeartBeat_Flag;
	int					tmrHB;
	int					HeartBeat_cnt;//used for debug
	
	/*  *3*  socket maintain thread ctl*/
	int					sockRecntFlag; //flag for socket recconect
	int					sockRecntCMD;

	/*  *4*  thread of communication with local module*/
	int					bandWidthCtlExeFlag;  //0:no need to clr,1:need to clr,3:cmd excuted
	int					autoDialerFlag;
	char				AutoDialerNum[NORMAL_BUFF_LEN];
	Local_Module_Sta_t	LocMod;
	//char SN[24]; already in aa_info
	int					APN_deliverFlag;
	char				APN_Info[NORMAL_BUFF_LEN];
	int					MSN_deliverFlag;//manual select net --MSN
	
	/*  *5*  Main process ctl var*/
	time_t				lastRstTime;
	int					VusimReinsert_Flag;
	int 				vusimReinserCnt;
	int 				vusimSwapFlag;
	int 				roamUpLoadFlow_Flag;
	int 				VsimType;
	int 				ProcRestart_flag;
	AccessAuth_Def		AA_Info;
	AccessAuth_Result_t AA_Result;

	/*  *6* Transfer transparently share */
	int					Apdu_Cnt;
	unsigned char		TT_cnt;
	unsigned char		TT_TimeOutCnt;
	int					TT_flag;
	int					TT_DataLen;
	unsigned char		TT_Buf[128];
	
	long long			IMEI;//add by lk 20151021
	char				sICCID[24];//add by lk 20161208

	int					fd[2];//add by 20161031 add for 7816

	int 				Power_off_ing_flag;
}Sys_Ctl_t;

typedef struct 
{
	int 				id;
	void 				*p_func;
	void 				*p_para;
	char 				*Name;
}Thread_Func_Id_list;

typedef struct{
	char				SN[16];
	char				sICCID[20];
}SN_Data;

extern Sys_Ctl_t SysCtl;

extern int SimFileRequest_main(int socket_fd, AccessAuth_Result_t *AA_Result);
extern int LocalSimFileCheck(AccessAuth_Result_t *AA_Result);
extern int AccessAuth_Info_PickUp(char *Src, AccessAuth_Def *Dst);

extern int TCP_AccessAuth_Main(int nfd, AccessAuth_Def *Src, TD_ComProtocl_RecvFrame_t *Dst, int *len);
extern int AccessAuth_Result_ANL(TD_ComProtocl_RecvFrame_t *Src, AccessAuth_Result_t *Dst, int len);
extern int Snd_Log_Head(char * p_SN, char * Log_Type, char *Log_Num, int Fd);
#endif
