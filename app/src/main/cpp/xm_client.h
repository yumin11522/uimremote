#ifndef __XM_CLIENT_H__
#define	__XM_CLIENT_H__


#include "pub.h"

#define DefaultIP_Addr				"218.17.107.11"
#define Domain_Name					"test.easy2go.cn"
#define DefaultLog_Addr				"120.24.221.252"
#define Defaultupdate_Addr			"120.24.221.252"
#define Android_Addr				"127.0.0.1"

#define DefaultPort					11111
#define DefaultLogPort				8070
#define DefaultUpdatePort			8070
#define AndroidPort			8848

#define FirmWareVersion				1705020000



typedef struct
{
	unsigned char Flag;
	unsigned char Leng;
	unsigned char Value[128];
}TT_Data_FromMiFi_Def;

typedef enum
{
	UnrecognizedDevSN = 1,
	NoDealForDevSN,//2
	OutOfService,//3
	NoSimUsable,//4
	MallocErr,//5
	IpChange,//add by lk 20150515 6
	LoginSuccess,//7
}SimAlloc_Err_Code;

typedef enum
{
	SockRecIdel = 0,
	SockReconnectStart,
	SockReconnecting,
	SockReconnected,
}SocketReconnectFlag_Value;

#define BillThread_ID			0
#define HeartBeatThread_ID		1
#define SockRecvThread_ID		2
#define SockSendThread_ID		3
#define ComWithLocalThread_ID	4
#define SockMaintain_ID			5
#define	UpdateSoft_ID			6
#define Power_ID				7
#define DEVICE_NAME				"/dev/simuart"  
#define TT_MAX_NUM				25//add by lk 20150929

#define TCP_RECV_TIMEOUT		7
#define HB_INTERVAL				60
#define LEN_OF_SN				16
#define	MAX_THREAD_CNT			6 //modify by lk 20150521

/*socket send ID define*/
#define	IDEL_ID					0
#define TT_ID					1
#define	HB_ID					2

#define READ_LOG_BUF_MAX		65*1024
#define TIME_OUT				3000 //add by 20160510

typedef enum 
{
    LOG_ROAM = 1,
    LOG_LOCAL = 2
}LOG_TYPE_En;

typedef enum
{
	LOGIN_RSP_OK = 0,
	LOGIN_RSP_TAG_ERROR = 1,
	LOGIN_RSP_ERROR_CODE = 2,
	LOGIN_RSP_JUMP = 3
 }LOGIN_RSP_RESULT_EN;

typedef enum
{
	TT_GUARD_IDLE = 0,
	TT_GUARD_TT_STATE = 1,
	TT_GUARD_TT_4MIN_NO_TT = 2,
	TT_GUARD_TT_4MIN_25_TT = 3,
	TT_GUARD_TT_NORMAL = 4
}TT_GUARD_STATE_EN;

typedef struct
{
	unsigned short 		MCC;
	unsigned char 		MNC;
	unsigned char 		Sig_Strength;
	unsigned int 		LAC;
	unsigned int 		CID;
	unsigned int 		Local_DataUsed;
}HB_NEW_t;

typedef struct{
	unsigned long long	SN;
	unsigned short		MCC;
	unsigned char		IMSI[9];
}Deal_t;

typedef struct
{
	unsigned char 		IMSI[9];
	char 				SN[15];
	unsigned int 		FirmWareVer;
}heartBeatWithData_t;

typedef struct ThreadGroupManage_t
{
	pthread_t 			pth_id;
	void 				*p_func;
	void 				*p_Para;
	char 				*Name;
}ThreadGrpMng_t;

typedef struct 
{
	int 				MCC;
	int 				MNC;
	int 				LAC;
	int 				CID;
	int 				sig_strenth;
}CellData_t;

extern int Send_Logout_Pack_To_Server(int Flag);
extern void ShutDown_NetWork(Sys_Ctl_t *p_SysCtl);
extern int start_main(void);
extern int Xm_Data_Init(const char* domainName, const char* ip, uint16_t port);

extern int TT_Process_t(Sys_Ctl_t *ctl);
#endif
