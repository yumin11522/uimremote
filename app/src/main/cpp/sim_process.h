#ifndef __SIM_PROCESS_H__
#define	__SIM_PROCESS_H__

#include <time.h>
#include "usim_drv.h"
#include "common.h"
#include "p_lib.h"

#define VSIM_AUTH_RECORD_MAX 		6
#define FCP_INDEX_FTYPE 			6
#define	FCP_INDEX_FSIZE_START 		2
#define	FCP_INDEX_MEMORY_SIZE_START 2
#define	FCP_INDEX_EF_STA 			11
#define	FCP_INDEX_EX_LEN 			12
#define	FCP_INDEX_RECORD_LEN 		14

typedef struct
{
	u8 								cnt;
	u8 								AuthResult[15];
	u8 								AuthRand[16];
	u8 								flag;
	u8 								AuthResult_Len;
	struct timeval 					snd_time;
	struct timeval 					rcv_time;
}VSIM_Auth_t;

typedef enum
{
	SIM_MF = 						0x01,
	SIM_DF = 						0x02,
	SIM_EF = 						0x04
}SIM_EF_DF_t;

typedef enum
{
	EF_TRANSPARENT =				0x00,
	EF_LINE = 						0x01,
	EF_CIR = 						0x03
}SIM_EF_STRUCTUER_t;

typedef enum
{
	OK_WITH_LE = 					0x9F,
	OK_WITHOUT_LE = 				0x90,
	MEMORY_STA = 					0x92,
	FILE_STA = 						0x94,
	SECURITY_STA = 					0x98,
	APP_STA_ERR_P3 = 				0x67,
	APP_STA_ERR_P1_P2 = 			0x6B,
	APP_STA_ERR_UNDEF = 			0x6D,
	APP_STA_ERR_INS   = 			0x6E,
	APP_STA_ERR_UNRESOLVEBLE = 		0x6F
}SIM_SW1;

typedef struct
{
	u8 Character;
	u8 DF_Num;
	u8 EF_Num;
}gsm_special_data_t;

typedef struct
{
	int 							Memory_size;
	u8 								ID[2];
	SIM_EF_DF_t 					Type;
	gsm_special_data_t 				GSM;
}MF_DF_FCP_t;

typedef struct
{
	SIM_EF_DF_t 					Type;
	u8 								ID[2];
	u8 								Sta;
	int 							Size;
	int 							record_len_each;
	int 							record_cnt;
	SIM_EF_STRUCTUER_t 				Structure;
}EF_FCP_t;

typedef struct 
{
	SIM_EF_DF_t F_type;
	MF_DF_FCP_t MF_DF;
	EF_FCP_t EF;
}FCP_ANL_RESULT_t;

extern VSIM_Auth_t VSIM_Auth[VSIM_AUTH_RECORD_MAX];
extern int Point_Vsim_Auth ;
extern void VSIM_MAIN_PROC(unsigned char *Buf, int iLen);

#endif
