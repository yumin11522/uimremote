#ifndef __FILE_REQUEST_H__
#define	__FILE_REQUEST_H__

#define	tag_fcb   	0x7A
#define	tag_fcp   	0x7B
#define	tag_fct   	0x7C
#define	tag_fch   	0x7D
#define	tag_atr   	0x7E

typedef struct{
	int 			Fn;
	unsigned char 	RequestFileTag;
	char 			*LocalFileName;
	unsigned long 	InvalidFileSize;
}File_List;

typedef struct
{
	unsigned char 	tag;
	unsigned char 	TotalBlock;
	unsigned char 	BlockNum;
	unsigned char 	*p_Data;
	unsigned char 	Cs;
}FileRequestFrameBody_t;

#define SIM_FILE_CHECK_EXIST  1
#define SIM_FILE_CHECK_ERR   -1
#define SIM_FILE_CHECK_NONE   0

#endif
