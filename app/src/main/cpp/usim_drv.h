#ifndef __USIM_DRIVER_H
#define __USIM_DRIVER_H

#define MAX_ATR_TRY						6
#define WAIT_ATR_TIMER     				200	//ms
#define USIM_OVER_TIMER     			500		//ms
#define	APDU_SCAN_TIMEOUT 				50 //ms
#define	APDU_SELECT_TIMEOUT 			100 //ms
#define	APDU_UPDATE_TIMEOUT 			100 //ms
#define APDU_AUTH_RAND_READ_TIMEOUT 	100
#define APDU_TRESPONSE_TIMEOUT 			100
#define	APDU_TERMINAL_PROFILE_TIMEOUT 	100

#define MAX_FILE_CODE_LEN				16	//=0x10
#define MAX_COMMENTS					16
#define NULL_POINT						0xFFFF
#define ATR_REBOOT_MAX_CNT				0

#define USIM_DIRECT  					0x3B
#define	USIM_INVERSE 					0x3F

#define TAMask 							0x10
#define TBMask 							0x20
#define TCMask 							0x40
#define TDMask 							0x80
#define KMask  							0x0F

#define FI_DEFAULT						372
#define DI_DEFAULT						1
#define INIT_WWT_T0						(9600+400)	// etu	(initial work waiting time) +400 to cover some slow card
#define TOUT_OFFSET						0x10		// apply a offset to all timeout settings (4*16 = 64 etu)
#define BGT_T1							22 			// etu	(block guard time)
#define NAD								0			// node address byte
#define SIM_DEFAULT_TOUT_VALUE     		0x983
#define SIM_CMD_TOUT_VALUE         		0x1400

#define LEN_MIN_T1						0
#define LEN_MAX_T1						254
#define USIM_IFSC_DEFAULT				32
#define USIM_IFSD_DEFAULT				32
#define USIM_CWT_DEFAULT				8203		// (11 + 1>>13) etu, depends on hardware
#define USIM_BWT_DEFAULT				15360		// (1<<4)*960
#define USIM_CLOCK_STOP_MSK 			0xA0      //TAi bit b8 b7, indicate XI, when T = 15
#define USIM_POW_CLASS_MSK				0x3f		// TAi bit b1~b6, indicate UI, when T = 15
#define USIM_PTS_PS1_MSK				0x10		//only have TA
#define USIM_PTS_PS0_T1					0x1			// select T1 protocol
#define USIM_NAD_DEFAULT				0x0

#define ATR_TA1_372_5					0x11
#define ATR_TA1_372_4					0x01
#define ATR_TA1_64						0x94		//F = 512, D = 8
#define ATR_TA1_32						0x95		//F=512, D=16
#define ATR_TA1_16						0x96		//F=512, D=32

#define USIM_RETRY						3
#define INDEX_COUNT						4		// the count of the wline and sline
#define MAX_BWI							9
#define MAX_CWI							16
#define SIM_TOTAL_FIFO_LEN				16		// excep 6208

#define USIM_MAX_HISTORY_CHARACTER 		15
#define USIM_1ST_CHARACTER				0x80	// class, indicate COMPACT-TLV
#define USIM_2ND_CHARACTER				0x31	// first info, data service
#define USIM_4TH__CHARACTER				0x73	// second info, capability

#define USIM_DMA_MAX_SIZE				260
#define USIM_GPT_TIMEOUT_PERIOD  		500	// x 10ms

#define USIM_CAL_TD_COUNT(data,index)	\
	{\
			if(data & TAMask) index++;\
	   		if(data & TBMask) index++;\
   			if(data & TCMask) index++;\
   	}

/********************define APUD related characters**********************/
#define APDU_HEAD_LEN 					sizeof(APDU_HEAD)
#define APDU_RESPONSE_LEN 				2 
#define APDU_RESPONSE_1					0x90
#define APDU_RESPONSE_2					0x60
#define APDU_RESPONSE_ERROR				0x60//???

#define	USIM_APDU1 						0x00
#define	USIM_APDU2 						0x80
#define	GSM_APDU  						0xA0
#define	PPS_APDU  						0xFF

#define CLA_MASK 						0xF0

#define SW1_OK_WITH_Le					0x61

#define SW1_OK_WITHOUT_Le				0x90
#define SW2_OK_WITHOUT_Le				0x00

#define SW1_OK_WITHOUT_Le2				0x91
#define SW2_OK_WITHOUT_Le2				0x40//部分联通卡实际数据为00，后续可根据FCH文件内容的长度进行修改


#define SW1_FILE_DONT_EXIST				0x6A
#define SW2_FILE_DONT_EXIST				0x82

#define SW1_INS_DONT_SUPPORT			0x6D
#define SW2_INS_DONT_SUPPORT			0x00

typedef struct{
	unsigned char CLA;
	unsigned char INS;
	unsigned char P1;
	unsigned char P2;
	unsigned char LcLe;
}APDU_HEAD;

typedef enum{
	SELECT_FILE	=						0xA4,
	GET_RESPONSE =						0xC0,	

	READ_BINARY =						0xB0,
	WRITE_BINARY =						0xD0,	
	ERASE_BINARY =						0x0E,
	UPDATE_BINARY =						0xD6,

	READ_RECORD =						0xB2,
	WRITE_RECORD =						0xD2,
	APPEND_RECORD =						0xE2,
	UPDATE_RECORD =						0xDC,
	SEARCH_RECORD =						0xA2,
	
	GET_DATA =							0xCA,	
	PUT_DATA =							0xDA,	

//启动从接口设备送入卡内的验证数据与卡内存储的引用数据
//(例如口令)进行比较
	VERIFY =							0x20,

	CHANGE_PIN =						0x24,
	DISABLE_PIN =						0x26,
	ENABLE_PIN =						0x28,
	UNBLOCK_PIN =						0x2C,

	INTERNAL_AUTHENTICATE =				0x88,//!!!!!!!!!!!!
	EXTERNAL_AUTHENTICATE =				0x82, 
	GET_CHALLENGE =						0x84,

	MANAGE_CHANNEL =					0x70,	//打开和关闭逻辑信道

	ENVELOPE =							0xC2,	

	TERMINAL_PROFILE =					0x10,
	TERMINAL_RESPONSE =					0x14,
	STATUS =							0xF2,
	FETCH =								0x12,	
	INS_NULL = 							0xFF
}APDU_INS;

#endif // _USIM_DRV_H_
