#ifndef __SIM_MAIN_H__
#define __SIM_MAIN_H__
#include "p_lib.h"

typedef enum 
{
	CMD_UART_BUF_CLR = 0,
	CMD_VUSIM_CTL ,
	CMD_RST_SCAN = 3,
	CMD_PWR_LED_SET,
	CMD_GET_INFO,
	CMD_SIM_UART_REINIT,
}cmds_of_ioctl;

typedef enum
{
	VSIM_ANL_IDEL = 0,
	VSIM_ANL_INSERT,
	VSIM_ANL_REMOVE,
	VSIM_ANL_START,
}VSIM_Anl_Sta_t;

typedef enum
{
	TT_Idel = 0,
	Snd_start,
	Snd_ing,
	TT_Snd_Err,
	TT_Recved,
}TT_flag_sta;

typedef enum 
{
	DEV_VERSION = 0,
	SIM_UART_RX_BYTES,
	LOCAL_PWR_STATUS,
	LOCAL_PWR_STATUS_E35,
}args_of_get_info;

extern void TT_Recv_Set(unsigned char *Dat, unsigned char DatLen, unsigned char RcvCnt);
extern int GetVisualSIM_TT_Data(unsigned char *TTbuf, unsigned char *Snd_cnt);
extern int Set_IMSI_Para(unsigned char *IMSI, TT_Data *para);
extern void ISO7816_ANL_Thread_Create(int fd);
extern void Set_VSIM_ANL_StateMacine(int state);
extern void SetTT_Snd_Sta(int Snd_cnt, int state);
extern void VisulSimProccess(unsigned char *recvBuf, int iRecvLen);
extern void VUSIM_init(void);
extern int VUSIM_ATR(void);
#endif
