#ifndef __SOCKET_BASE_H__
#define __SOCKET_BASE_H__

#include <sys/socket.h>
#include <sys/select.h>

#include <sys/time.h>

#include <sys/types.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

typedef unsigned char * PUCHAR;

#define CONN_TYPE_SHORT	0	/*������*/
#define CONN_TYPE_LONG	1	/*������*/

#define	ERR_SELECT		-10	/*select����ʧ��*/
#define	ERR_IOCTL		-11	/*ioctl����ʧ��*/
#define	ERR_SOCKET		-12	/*socket����ʧ��*/
#define	ERR_BIND		-13	/*bind����ʧ��*/
#define	ERR_LISTEN		-14	/*listen����ʧ��*/
#define	ERR_ACCEPT		-15	/*accept����ʧ��*/
#define	ERR_CONNECT		-16	/*connect����ʧ��*/
#define	ERR_RECV		-17	/*recv����ʧ��*/
#define	ERR_SEND		-18	/*send����ʧ��*/
#define	ERR_TIME_OUT	-19	/*��ʱ*/

extern int TCP_ConnectToServer(char* sIpAddr,int iPort);
extern int TCP_ConnectToServerTimeout(char* sIpAddr,int iPort,int iTimeout);

extern int TCP_RecvMessage(int iSockFd, unsigned char * sBuf, int iLen, int iType, int iTimeOut);
extern int TCP_RecvMessage_short(int iSockFd, unsigned char *sBuf, int iLen, int iType, int iTimeOut);
extern int TCP_RecvMessage_ms(int iSockFd, unsigned char *sBuf, int iLen, int iType, int iTimeOut);

extern int TCP_SendMessage(int iSockFd, const PUCHAR pucSendBuf, int nSendLen);
extern int SetSocketNonBlock(int iSock_fd);
extern int SetSocketBlock(int iSock_fd);

extern int SocketConnected(int sock);
#endif
