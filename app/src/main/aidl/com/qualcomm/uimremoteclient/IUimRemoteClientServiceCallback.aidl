// IUimRemoteClientServiceCallback.aidl
package com.qualcomm.uimremoteclient;

// Declare any non-default types here with import statements

interface IUimRemoteClientServiceCallback {
        /**
         * Send Event response to the client
         *
         * @param responseCode
         *   UIM_REMOTE_RESP_SUCCESS = 0;
         *   UIM_REMOTE_RESP_FAILURE = 1;
         * @return None
         */
        void uimRemoteEventResponse(in int slot, in int responseCode);

        /**
         * Send APDU response to the client
         *
         * @param responseCode
         *   UIM_REMOTE_RESP_SUCCESS = 0;
         *   UIM_REMOTE_RESP_FAILURE = 1;
         * @return None
         */
        void uimRemoteApduResponse(in int slot, in int responseCode);

        /**
         * Send an APDU indication requesting client to send an APDU to the response to
         * the remote UIM card
         *
         * @param slot
         *    UIM_REMOTE_SLOT0 =0;
         *    UIM_REMOTE_SLOT1 =1;
         *    UIM_REMOTE_SLOT2 =2
         *
         * @param apduCmd
         *    APDU command data sent to the remote UIM
         *
         * @return None
         */
        void uimRemoteApduIndication(in int slot, in byte[] apduCmd);

        /**
         * Send a connect indication requesting a connection to client
         *
         * @param slot
         *    UIM_REMOTE_SLOT0 =0;
         *    UIM_REMOTE_SLOT1 =1;
         *    UIM_REMOTE_SLOT2 =2
         *
         * @return None
         */
        void uimRemoteConnectIndication(in int slot);

        /**
         * Send an disconnect indication requesting a disconnection from client
         *
         * @param slot
         *    UIM_REMOTE_SLOT0 =0;
         *    UIM_REMOTE_SLOT1 =1;
         *    UIM_REMOTE_SLOT2 =2
         *
         * @return None
         */
        void uimRemoteDisconnectIndication(in int slot);

        /**
         * Send a power up indication requesting the remote UIM card to be powered up
         *
         * @param slot
         *    UIM_REMOTE_SLOT0 =0;
         *    UIM_REMOTE_SLOT1 =1;
         *    UIM_REMOTE_SLOT2 =2
         *
         * @return None
         */
        void uimRemotePowerUpIndication(in int slot);

        /**
         * Send a power down indication requesting the remote UIM card to be powered down
         *
         * @param slot
         *    UIM_REMOTE_SLOT0 =0;
         *    UIM_REMOTE_SLOT1 =1;
         *    UIM_REMOTE_SLOT2 =2
         *
         * @return None
         */
        void uimRemotePowerDownIndication(in int slot);
         /**
          * Send a reset indication requesting the remote UIM card to be reset
          *
          * @param slot
          *    UIM_REMOTE_SLOT0 =0;
          *    UIM_REMOTE_SLOT1 =1;
          *    UIM_REMOTE_SLOT2 =2
          *
          * @return None
          */
         void uimRemoteResetIndication(in int slot);

         /**
          * Send radio state to the client
          *
          * @param slot
          *    UIM_REMOTE_SLOT0 =0;
          *    UIM_REMOTE_SLOT1 =1;
          *    UIM_REMOTE_SLOT2 =2
          *
          * @param state
          *     RADIO_UNAVAILABLE = 0;
          *     RADIO_OFF =1;
          *     RADIO_ON = 2;
          * @return None
          */
         void uimRemoteRadioStateIndication(in int slot, in int state);
}
